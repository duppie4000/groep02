import pandas as pd

from queries import create_connection, get_exchanges, get_sentiment, add_new_entry, get_exchange_names, get_market_data, get_advies

pd.options.display.width = 0

def remove_id(data):
    new_list = []
    for line in data:
        entry = list(line)
        entry[0] = None
        entry = tuple(entry)
        new_list.append(entry)
    return new_list

def get_data(from_db):
    conn = create_connection(from_db)
    with conn:
        handelspaar = get_exchanges(conn)
        beurs = get_exchange_names(conn)
        sentiment = remove_id(get_sentiment(conn))
        koers = get_market_data(conn)
        advies = remove_id(get_advies(conn))




    return handelspaar, beurs, sentiment, koers, advies


def copy_data(from_db="live.db", to_db="inmon.db", initial = False):
    handelspaar, beurs, sentiment, koers, advies = get_data(from_db)

    print(sentiment)
    conn = create_connection(to_db)
    with conn:
        if initial:
            add_new_entry(conn, "Beurs", beurs)
            add_new_entry(conn, "Handelspaar", handelspaar)
        add_new_entry(conn, "Sentiment", sentiment)
        add_new_entry(conn, "Koers", koers)
        if advies:
            add_new_entry(conn, "Advies", advies)


def denormalize_koers(koers, handelspaar, beurs):
    handelspaar = pd.DataFrame(handelspaar, columns=['id', 'symbol1', 'symbol2', 'name', 'beurs_id'])
    beurs = pd.DataFrame(beurs, columns=['id', 'exchange'])
    handelspaar_beurs = handelspaar.join(beurs.set_index('id'), on="beurs_id")
    handelspaar_beurs.drop(columns='beurs_id', inplace=True)
    koers = pd.DataFrame(koers, columns=['timestamp', 'close', 'high', 'low', 'open', 'handelspaar_id'])
    koers = koers.join(handelspaar_beurs.set_index('id'), on="handelspaar_id")
    koers.drop(columns=["handelspaar_id"], inplace=True)
    koers.reset_index(inplace=True)
    koers.rename(columns={"index": "koers_id"}, inplace=True)

    return koers


def copy_to_kimball(from_db="staging.db", to_db="kimball.db"):
    handelspaar, beurs, sentiment, koers, advies = get_data(from_db)

    koers = denormalize_koers(koers, handelspaar, beurs)

    timestamps = koers['timestamp'].unique().tolist()
    datetime = pd.DataFrame(timestamps, columns=["timestamp"])
    datetime.reset_index(inplace=True)
    datetime.rename(columns={"index": "datetime_id"}, inplace=True)
    koers = koers.merge(datetime, left_on="timestamp", right_on="timestamp")

    datetime["timestamp"] = pd.to_datetime(datetime["timestamp"], unit='s', utc=False)

    datetime = datetime.assign(Date=datetime.timestamp.dt.date, Year=datetime.timestamp.dt.year,
                               Month=datetime.timestamp.dt.month, Day=datetime.timestamp.dt.day,
                               Time=datetime.timestamp.dt.time.astype(str))

    sentiment = pd.DataFrame(sentiment, columns=['sentiment_id', 'date', 'mentions', 'dominance'])
    sentiment['date'] = pd.to_datetime(sentiment['date'])
    sentiment = sentiment.assign(date=sentiment.date.dt.date)

    datetime = datetime.merge(sentiment[['sentiment_id', 'date']], left_on='Date', right_on='date', how="left")

    advies = pd.DataFrame(
        {'advice': None, 'datetime_id': datetime['datetime_id'], 'sentiment_id': datetime['sentiment_id']})
    advies = advies.merge(koers[['koers_id', 'datetime_id']], left_on='datetime_id', right_on='datetime_id')
    advies.reset_index(inplace=True)

    sentiment.drop(columns='date', inplace=True)
    datetime.drop(columns=["timestamp", 'Date', 'date', 'sentiment_id'], inplace=True)
    koers.drop(columns=['datetime_id', 'timestamp'], inplace=True)


    conn = create_connection(to_db)
    with conn:
        add_new_entry(conn, "Koers", list(koers.itertuples(index=False, name=None)))
        add_new_entry(conn, "Datetime", list(datetime.itertuples(index=False, name=None)))
        add_new_entry(conn, "Sentiment", list(sentiment.itertuples(index=False, name=None)))
        add_new_entry(conn, "Advies", list(advies.itertuples(index=False, name=None)))

def remove_duplicates(db, kimball = False):
    conn = create_connection(db)
    with conn:
        cur = conn.cursor()
        if not kimball:
            cur.execute("DELETE FROM Koers WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Koers GROUP BY time, close, high, low, open, handelspaar_id)")
            cur.execute("DELETE FROM Sentiment WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Sentiment GROUP BY date, mentions, dominance)")
            cur.execute("DELETE FROM Advies WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Advies GROUP BY time, advice)")
        else:
            cur.execute("DELETE FROM Koers WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Koers GROUP BY close, high, low, open, symbol1, symbol2, name, exchange)")
            cur.execute("DELETE FROM Sentiment WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Sentiment GROUP BY mentions, dominance)")
            cur.execute("DELETE FROM Advies WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Advies GROUP BY advice, datetime_id, sentiment_id, koers_id)")
            cur.execute(("DELETE FROM Datetime WHERE ROWID NOT IN (SELECT MIN(ROWID) FROM Datetime GROUP BY year, month, day, time)"))
