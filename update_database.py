import logging
from datetime import date
from datetime import timedelta

import san
import yfinance as yf

import cryptocompare as market
from queries import get_sentiment, get_exchanges, get_recent, get_exchange_name, update_entry, add_new_entry, \
    create_connection, update_sentiment_entry

API_KEY = 'afc45f1b22ffb957f7722ed7a14c903f1abc30b7a3ad7f13003d3c7bcc9544b1'  # API-Key voor cryptocompare
san.ApiConfig.api_key = 'opqmzdoxfjb5eotx_6pwfz3xhupzx344m'


# Schoon response op van cryptocompare-api
def _clean_response(response, koersbeurs_id):
    """
    Schoon response op van cryptocompare-api
    :param response: De response gegeven door de cryptocompare API
    :param koersbeurs_id: het id van het koersbeurs paar om toe te voegen aan de koersdata
    :return: data: list met data.
    """
    data = []
    for line in response:
        line = str(line)
        line = line[1:-1].split()
        line = line[1::2]
        line = [x.strip(',') for x in line]
        line = line[0:5]
        try:
            line = list(map(float, line))
            line.append(koersbeurs_id)
            line = tuple(line)
            data.append(line)
        except:
            pass

    return data


def get_new_crypto_data(coin, curr, exchange, handelspaar_id):
    '''
    Haalt de afgelopen paar uur van data op van cryptocompare
    :param coin: op te vragen symbool van een cryptocurrency, bijvoorbeeld ETH
    :param curr: bijbehorende currency waarmee wordt gehandeld, bijvoorbeeld USD
    :param exchange: naam van de beurs waar data van wordt opgevraagd. Bijvoorbeeld binance
    :param handelspaar_id: de id van het handelspaar waar de data over moet worden opgevraagd
    :return: opgeschoonde response van de cryptocompare api met daarin de nieuwe data
    '''
    response = market.get_historical_price_hour(coin, curr, 5, exchange, api_key=API_KEY)
    try:
        return _clean_response(response, handelspaar_id)
    except:
        return _clean_response(market.get_historical_price_hour(coin, "USDT", 5, exchange, api_key=API_KEY),
                               handelspaar_id)


def get_yfinance_data(handelspaar_id, stock):
    """
    Parameters
    ----------
    ticker_name : TYPE, optional
        DESCRIPTION. Naam van de ticker die je wilt opvragen. standaard is "^AEX".
        Via de yahoo finance website kan worden achterhaald wat de naam van de ticker
        van een aandeel is.

    """
    ticker = yf.Ticker(stock)
    hist = ticker.history(period="1d", interval="60m")

    hist = hist.drop(columns=["Stock Splits", "Dividends", "Volume"])
    hist = hist.reset_index()
    hist.columns = ["time", "close", "high", "low", "open"]

    temp = hist.tail(1)["time"].dt.minute == 0

    if not temp.values[0]:
        hist.drop(hist.tail(1).index, inplace=True)

    hist['time'] = hist['time'].astype('int64') // 1e9
    hist["koersbeurs_id"] = handelspaar_id
    return list(hist.itertuples(index=False, name=None))


def get_new_trends():
    from_date = str(date.today() - timedelta(days=7))
    df = san.get(
        "social_volume/ethereum",
        from_date=from_date,
        interval="1d"
    )

    social_dominance = san.get(
        "social_dominance/ethereum",
        from_date=from_date,
        interval="1d"
    )

    df = df.reset_index()
    social_dominance = social_dominance.reset_index()
    df = df.join(social_dominance.set_index("datetime"), on="datetime")
    df.columns = ['date', 'mentions', 'dominance']
    df['date'] = df['date'].apply(str)
    new_data = list(df.itertuples(index=False, name=None))
    return new_data


def update_sentiment(conn):
    with conn:
        new_data = get_new_trends()
        old_data = get_sentiment(conn)
        old_dates = []

        for x in old_data:
            old_dates.append(x[1])

        for x in new_data:
            if x not in old_data:
                if x[0] in old_dates:
                    update_sentiment_entry(conn, x)
                else:
                    x = (None,) + x
                    add_new_entry(conn, "Sentiment", x)


def update_koers(conn):
    updates, new = 0, 0
    with conn:
        pairs = get_exchanges(conn)
        for i, row in enumerate(pairs, start=1):
            print(i, "/", len(pairs))
            koersbeurs_id, coin, curr, name, exchange_id = row

            latest_entries = get_recent(conn, koersbeurs_id)
            exchange = get_exchange_name(conn, exchange_id)

            try:
                new_data = get_new_crypto_data(coin, curr, exchange, koersbeurs_id)
            except:
                new_data = get_yfinance_data(koersbeurs_id, coin)

            for x in new_data:
                if x not in latest_entries:
                    for y in latest_entries:
                        if x[0] in y:
                            update_entry(conn, x)
                            updates += 1
                            break
                    else:
                        add_new_entry(conn, "Koers", x)
                        new += 1

    logging.basicConfig(filename='updates.log', format='%(asctime)s - %(message)s', level=logging.INFO)
    logging.info(f"{updates} records bijgewerkt.")
    logging.info(f"{new} records toegevoegd.")


def main():
    print("Database wordt bijgewerkt. Moment geduld a.u.b...")
    # database = "C:/Users/Gebruiker2/Desktop/live.db"
    database = "live.db"
    conn = create_connection(database)
    with conn:
        update_koers(conn)
        update_sentiment(conn)


if __name__ == '__main__':
    main()
