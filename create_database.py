import os
from datetime import datetime as dt

import pandas as pd
import san
import yfinance as yf

import cryptocompare as market
from queries import create_advies, create_connection

# https://github.com/duppie4000/cryptocompare
# Fork van de cryptocompare-api te vinden in pip, door Michiel geupdated met extra benodigde features.

san.ApiConfig.api_key = 'opqmzdoxfjb5eotx_6pwfz3xhupzx344m'

API_KEY = 'afc45f1b22ffb957f7722ed7a14c903f1abc30b7a3ad7f13003d3c7bcc9544b1'  # API-Key voor cryptocompare
beurzen = []
handelsparen = []

# Pas hier de exchanges aan waar je data van wilt opvragen
exchange_list = ["Binance", "Coinbase", "Bitfinex", "Liquid"]

# Vul hier de aandelen in waar je data van wilt opvragen
# Standaardwaardes: AEX-INDEX, DOW JONES INDEX, NASDAQ, Goud, Olie
stock_list = ["^AEX", "^DJI", "^IXIC", "GC=F", "CL=F"]


def _clean_response(response, data):
    """
    Schoon response op van cryptocompare-api
    :param response: De response gegeven door de cryptocompare API
    :param data: list met huidige data. Nodig omdat cryptocompare maar maximaal 2000 entries in 1 query teruggeeft,
                 in deze list wordt alle data opgeschoonde verzameld voordat het naar een dataframe wordt geschreven.
    :return: data: list met data van een exchange.
             toTs: timestamp die aangeeft tot wanneer de data moet lopen. Nodig om verder terug te gaan dan 2000 entries.
             lowest: de waarde van de koers. Als deze 0 is, gaan we er vanuit dat er geen data meer te vinden is voor
                     deze exchange en kunnen we verder met de volgende.
    """
    for line in reversed(response):
        line = str(line)
        line = line[1:-1].split()
        line = line[1::2]
        line = [x.strip(',') for x in line]
        toTs = line[0]
        lowest = float(line[1])
        if lowest <= 0:
            break
        try:
            line = list(map(float, line))
            data.append(line)
        except:
            pass

    return data, toTs, lowest


def get_market_history(coin="ETH", curr="USDT", limit=2000, exchange='binance', toTs=dt.now()):
    """

    Vraagt alle geschiedenis op van een crypto-beurs van de cryptocompare-api.
    :param coin: op te vragen symbool van een cryptocurrency, bijvoorbeeld ETH
    :param curr: bijbehorende currency waarmee wordt gehandeld, bijvoorbeeld USD
    :param limit: aantal rijen om op te vragen. (Max. 2000)
    :param exchange: naam van de beurs waar data van wordt opgevraagd. Bijvoorbeeld binance
    :param toTs: timestamp van meest recente benodigde data. Nodig om de volledige historie op te kunnen vragen.
    :return: Return False als coin/curr paar niet bestaat, anders return True

    """
    data = []
    try:
        if limit == 2000:
            while True:
                response = market.get_historical_price_hour(coin, curr, limit, exchange, toTs, API_KEY)
                data, toTs, lowest = _clean_response(response, data)
                if lowest <= 0:
                    break
        else:
            response = market.get_historical_price_hour(coin, curr, limit, exchange, toTs, API_KEY)
            data, toTs, lowest = _clean_response(response, data)

        df = pd.DataFrame(data, columns=['time', 'close', 'high', 'low', 'open', 'volumefrom', 'volumeto'])
        df = df.drop(columns=["volumefrom", "volumeto"])
        df['time'] = df['time'].astype(int)
        df = df.drop(df[(df.open <= 0) & (df.close <= 0) & (df.high <= 0) & (df.low <= 0)].index)

        if curr == 'USDT':
            curr = 'USD'

        export_data(df, exchange, "Ethereum", curr, coin, "live.db")
    except:
        return False
    return True


# 
def get_all_exchanges(crypto_exchanges: list = None, stocks: list = None, filename="live.db", limit=1000, period="7d"):
    """
    Voer get_market_history en get_yfinance_data uit voor alle gewenste crypto_exchanges en aandelen

    Parameters
    ----------
    :param crypto_exchanges : list, optional
        DESCRIPTION. Optioneel kan een lijst van exchanges worden meegegeven.
        Voorwaarde is dat cryptocompare gegevens heeft van de betreffende exchanges
        Indien niets wordt meeegegeven wordt een lijst van 'Top tier' exchanges
        gebruikt, opgehaald van cryptocompare.
        Zowel ETH/USD als ETH/EUR wordt opgevraagd.
        Default-waarde: None

    :param stocks : list, optional
        DESCRIPTION. Eventueel kan een lijst met tickers worden opgegeven voor
        traditionele beurzen. Voorwaarde is dat ze beschikbaar zijn via
        yahoo finance. voorbeeld: ["DOW","^AEX"]
        Indien er niets wordt meegegeven wordt alleen ^AEX op

    """
    global beurzen, handelsparen

    print("Data ophalen van ", len(crypto_exchanges), " crypto beurzen. Dit kan een paar minuten duren")

    for i, exchange in enumerate(crypto_exchanges, start=1):
        print(i, "/", len(crypto_exchanges))
        if not get_market_history(exchange=exchange, coin='ETH', curr='USDT', limit=limit):
            get_market_history(exchange=exchange, coin='ETH', curr='USD', limit=limit)
        get_market_history(exchange=exchange, coin='ETH', curr='EUR', limit=limit)

    print("beursdata ophalen...")
    if stocks is not None:
        for i in stocks:
            hist, ticker = get_yfinance_data(i, period=period)
            export_data(hist, ticker.info["exchange"], ticker.info["shortName"], ticker.info["currency"],
                        ticker_name=i, filename=filename)
    else:
        hist, ticker = get_yfinance_data()
        export_data(hist, ticker.info["exchange"], ticker.info["shortName"], ticker.info["currency"],
                    ticker_name="^AEX", filename=filename)

    conn = create_connection(filename)
    with conn:
        cur = conn.cursor()
        cur.execute('''CREATE TABLE Sentiment  (id INTEGER PRIMARY KEY, date DATETIME NOT NULL, mentions 
                    INTEGER, dominance REAL);''')
        get_sanpy_trends().to_sql("Sentiment", con=conn, if_exists='append', index=False)
        create_advies(conn)
    print("Klaar!")
    beurzen = []
    handelsparen = []


# Exporteer data naar een SQLite database
def export_data(df, exchange, stock, curr, ticker_name, filename):
    global beurzen, handelsparen
    conn = create_connection(filename)

    if exchange not in beurzen:
        beurzen.append(exchange)
        data = {'id': [beurzen.index(exchange)], 'exchange': [exchange]}
        beurs = pd.DataFrame(data=data)
        beurs.to_sql("Beurs", if_exists='append', con=conn, index=False)

    data = (stock, curr, beurzen.index(exchange))

    if data not in handelsparen:
        handelsparen.append(data)
        data_dict = {'id': [handelsparen.index(data)], 'symbol1': [ticker_name], 'symbol2': [curr], 'name': [stock],
                     'exchange_id': [beurzen.index(exchange)]}
        handelspaar = pd.DataFrame(data=data_dict)

        handelspaar.to_sql("Handelspaar", con=conn, if_exists='append', index=False)

    df['handelspaar_id'] = handelsparen.index(data)
    df.to_sql("Koers", con=conn, if_exists='append', index=False)


# Haal data op van de yahoo-finance API.
def get_yfinance_data(ticker_name="^AEX", period="7d"):
    """
    Parameters
    ----------
    ticker_name : TYPE, optional
        DESCRIPTION. Naam van de ticker die je wilt opvragen. standaard is "^AEX".
        Via de yahoo finance website kan worden achterhaald wat de naam van de ticker
        van een aandeel is.

    """
    ticker = yf.Ticker(ticker_name)
    hist = ticker.history(period=period, interval="60m")

    hist = hist.drop(columns=["Stock Splits", "Dividends", "Volume"])
    hist = hist.reset_index()
    hist.columns = ["time", "close", "high", "low", "open"]

    temp = hist.tail(1)["time"].dt.minute == 0

    if not temp.values[0]:
        hist.drop(hist.tail(1).index, inplace=True)

    hist['time'] = hist['time'].astype('int64') // 1e9  # Omzetting van timestamp naar seconds since epoch
    return hist, ticker


def get_sanpy_trends():
    df = san.get("social_volume/ethereum",
                 interval="1d")

    social_dominance = san.get(
        "social_dominance/ethereum",
        interval="1d"
    )

    df = df.reset_index()
    social_dominance = social_dominance.reset_index()

    df = df.join(social_dominance.set_index("datetime"), on="datetime")
    df = df.reset_index()

    df.columns = ['id', 'date', 'mentions', 'dominance']
    return df


def main(exchange_list, stock_list):
    print("WAARSCHUWING: Dit script uitvoeren wist de huidige database.")
    keuze = input("Verdergaan? J / N\n")
    if keuze.lower() != 'j':
        print("Script wordt niet uitgevoerd.")
    else:
        if os.path.exists('live.db'):
            print("Database wordt verwijderd")
            os.remove('live.db')

    get_all_exchanges(crypto_exchanges=exchange_list, stocks=stock_list)


if __name__ == '__main__':
    main(exchange_list, stock_list)
