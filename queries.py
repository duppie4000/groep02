import os
import sqlite3 as sqlite
from datetime import datetime
from sqlite3 import Error


def create_connection(db):
    """
    :param db: naam van de database
    :return: conn: de verbinding met de database
    """
    conn = None
    try:
        conn = sqlite.connect(db)
    except Error as e:
        print(e)

    return conn


def get_exchanges(conn):
    """
    selecteer alle handelsparen van de database

    :param conn: de verbinding met de database
    :return: rows: het resultaat van de query

    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM Handelspaar")

    rows = cur.fetchall()
    return rows


def get_recent(conn, handelspaar_id):
    """
    Haal de laatste 100 entries van elk handelspaar op van de database

    :param conn: de verbinding met de database
    :param handelspaar_id: het id van het betreffende handelspaar in de database
    :return: het resultaat van de query

    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM Koers WHERE handelspaar_id == ? ORDER BY time DESC LIMIT 100", (handelspaar_id,))

    rows = cur.fetchall()
    return rows


def get_exchange_name(conn, exchange_id):
    '''
    Haal de naam op van een exchange aan de hand van het id
    :param conn: de verbinding met de database
    :param exchange_id: het id van de beurs
    :return: rows[0][0]: een string met de naam van de exchange
    '''
    cur = conn.cursor()
    cur.execute("SELECT exchange FROM Beurs WHERE id == ?", (exchange_id,))

    rows = cur.fetchall()
    return rows[0][0]


def update_entry(conn, updated_line):
    '''
    Werkt bestaande entries uit de database bij met nieuwe informatie
    :param conn: de verbinding met de database
    :param updated_line: de nieuwe regel die in de database moet worden geplaatst
    '''
    cur = conn.cursor()
    time, close, high, low, open, koersbeurs_id = updated_line

    cur.execute("UPDATE Koers SET close=?, high=?, low=?, open=?"
                "WHERE handelspaar_id == ? AND time == ?", (close, high, low, open, koersbeurs_id, time))


def _format_string(values):
    value_string = "("
    for x in range(values):
        value_string += "?"
        if x != values - 1:
            value_string += ","
    value_string += ")"

    return value_string


def add_new_entry(conn, table_name, new_line):
    '''
    Voegt een nieuwe regel toe aan de database
    :param conn: de verbinding met de database
    :param new_line: de nieuwe regel om toe te voegen aan de database
    :return:
    '''
    if not new_line:
        return
    cur = conn.cursor()

    if isinstance(new_line, str) or isinstance(new_line, tuple):
        values = len(new_line)
    else:
        values = len(new_line[0])

    total_string = "INSERT INTO " + table_name + " VALUES " + _format_string(values)
    if isinstance(new_line, list):
        cur.executemany(total_string, new_line)
    else:
        cur.execute(total_string, new_line)


def get_sentiment(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM Sentiment")
    return cur.fetchall()


def update_sentiment_entry(conn, entry):
    cur = conn.cursor()
    date = (entry[0],)
    entry = entry + date
    cur.execute("UPDATE Sentiment SET date=?, mentions=?, dominance=?"
                "WHERE date == ?", entry)


def get_exchange_names(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM Beurs")

    rows = cur.fetchall()
    return rows


def get_market_data(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM Koers")

    rows = cur.fetchall()
    return rows


def clear_db(conn=None, db_name: str = "live.db"):
    if conn is None:
        conn = create_connection(db_name)

    with conn:
        cur = conn.cursor()
        cur.execute("DELETE FROM Koers")
        cur.execute("DELETE FROM Sentiment")
        cur.execute("DELETE FROM Advies")
        if "kimball" not in db_name.lower():
            cur.execute("DELETE FROM Beurs")
            cur.execute("DELETE FROM Handelspaar")
        else:
            cur.execute("DELETE FROM Datetime")


def create_advies(conn):
    cur = conn.cursor()
    cur.execute("CREATE TABLE Advies (id INTEGER PRIMARY KEY, time DATETIME, advice TEXT)")


def record_updates(conn, from_db, to_db, notes=""):
    with conn:
        cur = conn.cursor()
        changes = (None, datetime.now(), from_db, to_db, True, notes)
        cur.execute("INSERT INTO VERANDERINGEN VALUES(?,?,?,?,?,?)", changes)

def get_file_locations():
    here = os.path.dirname(os.path.abspath(__file__))
    live = os.path.join(here, 'live.db')
    staging = os.path.join(here, 'staging.db')
    inmon = os.path.join(here, 'inmon.db')
    kimball = os.path.join(here, 'kimball.db')
    return live, staging, inmon, kimball

def get_advies(conn):
    with conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM Advies")
        rows = cur.fetchall()
    return rows