import os

from create_database import get_all_exchanges
from dwh import copy_data, copy_to_kimball
from queries import create_connection, clear_db, record_updates, get_file_locations

# Pas hier de exchanges aan waar je data van wilt opvragen
exchange_list = ["Binance", "Coinbase", "Bitfinex", "Liquid"]

# Vul hier de aandelen in waar je data van wilt opvragen
# Standaardwaardes: AEX-INDEX, DOW JONES INDEX, NASDAQ, Goud, Olie
stock_list = ["^AEX", "^DJI", "^IXIC", "GC=F", "CL=F"]


def run():
    live, staging, inmon, kimball = get_file_locations()

    if os.path.exists(live):
        print("Database wordt verwijderd")
        os.remove(live)
        clear_db(db_name=inmon)
        clear_db(db_name=staging)
        clear_db(db_name=kimball)

    get_all_exchanges(exchange_list, stock_list, live, limit=2000, period="365d")
    conn = create_connection(staging)
    with conn:
        clear_db(conn)

    copy_data(from_db=live, to_db=staging, initial=True)
    record_updates(conn, "Live", "Staging", "Initial Load")

    copy_data(from_db=staging, to_db=inmon, initial=True)
    record_updates(conn, "Staging", "Inmon", "Initial Load")

    copy_to_kimball()
    record_updates(conn, "Staging", "Kimball", "Initial Load")

    os.remove(live)
    get_all_exchanges(exchange_list, stock_list, live, limit=168, period="1wk")


print("Door dit script uit te voeren wordt alle huidige data in alle databases verwijdert.")
keuze = input("Verdergaan? J / N\n")

if keuze.lower() != 'j':
    print("Script wordt niet uitgevoerd.")
else:
    run()
