import os

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.io as pio
import sqlite3
import pandas as pd
import numpy as np
import datetime
from dash.dependencies import Input, Output
from queries import add_new_entry, create_connection

# Create your connection.
from update_database import update_sentiment, update_koers

here = os.path.dirname(os.path.abspath(__file__))
live = os.path.join(here, 'live.db')
cnx = create_connection(live)

plotly_template = pio.templates["plotly_dark"]

def ExpMovingAverage(values, window):
    weights = np.exp(np.linspace(-1., 0., window))
    weights /= weights.sum()
    
    a = np.convolve(values, weights)[:len(values)]
    a[:window]=a[window]
    return a


handelspaar_id = "3"
cnx = create_connection(live)
with cnx:
    df = pd.read_sql_query("SELECT * FROM Koers WHERE handelspaar_id=(?) LIMIT 800", cnx, params=handelspaar_id)
    df['time'] = pd.to_datetime(df['time'], unit='s')

hoogste24 = df['high'].iloc[24:].max()
laagste24 = df['low'].iloc[24:].min()
laatstePrijs = df['high'].iloc[1]


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div([
    dcc.Interval(
            id='interval-component',
            interval=1*100000, # in milliseconds
            n_intervals=0),
            
html.Div([
    html.Div([
        html.Div([
        html.Div([
                html.Img(src='/assets/white-trading-icons-lg-04.png')
            ], className='logo'),
        html.Div([
            html.P('Ethereum trading dasboard met advies. Gemaakt door Michiel Flaton, Victor Hovius en Ben van Kampen')
            ], className='Tekst'),
        html.Div([
            html.H1(
                    datetime.datetime.now().strftime('%d-%m-%y')),
             ], className='Tijd'),
        
            html.Div([
                html.Div([
                    html.Div([
                        html.P('Laatste Prijs : ')
                        ], className='six columns'),
                    html.Div([
                        html.P(id='laatstePrijs')
                        ], className='six columns')
                    ], className='info'),
                ], className='row'),
            
            html.Div([
                html.Div([
                    html.Div([
                        html.P('24H High : ')
                        ], className='six columns'),
                    html.Div([
                        html.P(id='hoogste24')
                        ], className='six columns')
                    ], className='info'),
                ], className='row'),
                        
            html.Div([
                html.Div([
                    html.Div([
                        html.P('24H Low : ')
                        ], className='six columns'),
                    html.Div([
                        html.P(id='laagste24')
                        ], className='six columns')
                    ], className='info'),
                ], className='row'),
            
            html.Div([
            html.H1('ADVIES'), 
            html.P('Op basis van onze technische analyse dat gebruikt maakt van exponential moving averages (EMA) komen wij tot het volgende advies:'),
            html.H2(id='Adviesje')
            ], className='Advies'),
            
            html.Div([

                ]),
                        

        ], className='three column left panel'),
        ], className='three columns'),
    
      
    html.Div([
        html.Div([
            html.Div([
            html.H2('ETHEREUM / EURO'),
            ], className='BANNER'),
            html.Div([dcc.Graph(id='ETH')
                      ]),
        ], className='Ethereum Koers'),
        
    html.Div([
        html.Div([
            
            
        html.Div([
            html.Div([
            html.H2('AEX / EURO'),
            ], className='BANNER'),
            html.Div([dcc.Graph(id='AEX')], className='AEX / EURO'),
                ], className='six  columns'),
            
        html.Div([
            html.Div([
            html.H2('DOMINANTIE SENTIMENT'),
            ], className='BANNER'),
            html.Div([dcc.Graph(id='Sent')], className='SENTIMENT')
                ], className='six columns'),
        

            
            
            ], className='duo'),
        ], className='row'),
    

    
    ], className='nine columns'),
    
], className='row')

])



@app.callback(Output('Adviesje', 'children'),
              [Input('interval-component', 'n_intervals')])
def runAdvies(n):
    datum_vandaag = datetime.datetime.now()
    cnx = create_connection(live)
    with cnx:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM Advies")
        rows = cur.fetchall()
        print(rows)
            
        if not rows or (str(datum_vandaag.date()) not in rows[-1] and datum_vandaag.hour == 7):  
            if (ExpMovingAverage(df['close'], 3)[1]) >= df['close'].iloc[1]:
                advies = 'Kopen'            
            else: 
                advies = 'Verkopen' 
                
            line = (None, datum_vandaag.date(), advies)
            print(line)
            add_new_entry(cnx, "Advies", line)
            
        else:
            advies = rows[-1][2]
                
    return advies

@app.callback(Output('ETH', 'figure'),
              [Input('interval-component', 'n_intervals')])
def ethereum_koers(n):
    global hoogste24, laagste24, laatstePrijs
    handelspaar_id = "3"
    cnx = create_connection(live)
    with cnx:
        df = pd.read_sql_query("SELECT * FROM Koers WHERE handelspaar_id=(?) LIMIT 800", cnx, params=handelspaar_id)
        df['time'] = pd.to_datetime(df['time'], unit='s')

    hoogste24 = '€ ', df['high'].iloc[0:23].max()
    laagste24 = '€ ', df['low'].iloc[0:23].min()
    laatstePrijs = '€ ', df['high'].iloc[0]

    figure = {
        'data': [go.Candlestick(x=df['time'],
                                open=df['open'],
                                high=df['high'],
                                low=df['low'],
                                close=df['close']),
                 go.Scatter(x=df['time'], y=ExpMovingAverage(df['close'], 3), line=dict(color='blue', width=1))
        ],
        'layout': {
              'xaxis': dict(showgrid=False, rangeslider=dict(visible=False), rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1d',
                         step='day',
                         stepmode='backward'),
                    dict(count=3,
                         label='3d',
                         step='day',
                         stepmode='backward'),
                    dict(count=5,
                         label='5d',
                         step='day',
                         stepmode='backward'),
                    dict(count=7,
                         label='7d',
                         step='day',
                         stepmode='backward'),
                    dict(step='all')
                ]))),
            'yaxis': dict(autoscale=True, gridcolor='383129'),
            'paper_bgcolor' : 'rgb(34,37,43)',
            'plot_bgcolor' : 'rgb(34,37,43)',
        }
    }
    
    return figure


@app.callback(Output('AEX', 'figure'),
              [Input('interval-component', 'n_intervals')])
def aex_graph(n):
    cnx = create_connection(live)

    with cnx:
        dfaex = pd.read_sql_query("SELECT * FROM Koers WHERE handelspaar_id=8 LIMIT 800", cnx)
        dfaex['time'] = pd.to_datetime(df['time'], unit='s')

    figure = {
        'data': [go.Candlestick(x=dfaex['time'],
                                open=dfaex['open'],
                                high=dfaex['high'],
                                low=dfaex['low'],
                                close=dfaex['close'])],
        'layout': {
            'xaxis': dict(showgrid=False, rangeslider=dict(visible=False), rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1d',
                         step='day',
                         stepmode='backward'),
                    dict(count=3,
                         label='3d',
                         step='day',
                         stepmode='backward'),
                    dict(count=5,
                         label='5d',
                         step='day',
                         stepmode='backward'),
                    dict(count=7,
                         label='7d',
                         step='day',
                         stepmode='backward'),
                    dict(step='all')
                ]))),
            'yaxis': dict(autoscale=True, gridcolor='383129'),
            'paper_bgcolor' : 'rgb(34,37,43)',
            'plot_bgcolor' : 'rgb(34,37,43)',
        }
    }
    return figure


@app.callback(Output('Sent', 'figure'),
              [Input('interval-component', 'n_intervals')])
def sentiment_graph(n):
    cnx = create_connection(live)

    with cnx:
        sentiment = pd.read_sql_query("SELECT * FROM Sentiment", cnx)

    figure = {
        'data': [go.Scatter(x=sentiment['date'], y=sentiment['dominance'],
                            mode='lines+markers',
                            name='lines+markers',)],

        'layout': {
            'xaxis': dict(showgrid=False, rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1d',
                         step='day',
                         stepmode='backward'),
                    dict(count=3,
                         label='3d',
                         step='day',
                         stepmode='backward'),
                    dict(count=5,
                         label='5d',
                         step='day',
                         stepmode='backward'),
                    dict(count=7,
                         label='7d',
                         step='day',
                         stepmode='backward'),
                    dict(step='all')
                ]))),
            'yaxis': dict(autoscale=True, gridcolor='383129'),
            'paper_bgcolor' : 'rgb(34,37,43)',
            'plot_bgcolor' : 'rgb(34,37,43)',
        }
    }
    return figure

@app.callback(Output('laatstePrijs', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_laatstePrijs(n):
    return laatstePrijs

@app.callback(Output('laagste24', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_laatstePrijs(n):
    return laagste24

@app.callback(Output('hoogste24', 'children'),
              [Input('interval-component', 'n_intervals')])
def update_laatstePrijs(n):
    return hoogste24


if __name__ == '__main__':
    app.run_server()
