
from datetime import timedelta, datetime

from dwh import copy_data, copy_to_kimball, remove_duplicates
from queries import get_file_locations, create_connection, clear_db, record_updates


def remove_old_from_live(live):
    conn = create_connection(live)
    with conn:
        cur = conn.cursor()
        last_week = datetime.today() - timedelta(days=7)
        last_week = last_week.timestamp()
        cur.execute("DELETE FROM KOERS WHERE time < ?", (last_week, ))


live, staging, inmon, kimball = get_file_locations()
conn = create_connection(staging)
clear_db(db_name = staging)
copy_data(from_db="live.db", to_db="staging.db")
record_updates(conn, "Live", "Staging", "Update")
copy_data(from_db="staging.db", to_db="inmon.db")
record_updates(conn, "Staging", "Inmon", "Update")
copy_to_kimball(from_db="staging.db", to_db="kimball.db")
record_updates(conn, "Staging", "Kimball", "Update")
remove_duplicates(inmon)
remove_duplicates(kimball, kimball=True)
remove_old_from_live(live)
